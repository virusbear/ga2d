package virusbear.ga2d

import processing.core.PApplet
import processing.core.PConstants.CENTER

data class Rectangle(val x: Double, val y: Double, val w: Double, val h: Double) {

    fun contains(p: Vertex): Boolean {
        return  (p.x >= x - w) &&
                (p.x <= x + w) &&
                (p.y >= y - h) &&
                (p.y <= y + h)
    }

    fun intersects(rect: Rectangle): Boolean {
        return !(rect.x - rect.w > x + w ||
                 rect.x + rect.w < x - w ||
                 rect.y - rect.h > y + h ||
                 rect.y + rect.h < y - h)
    }
}

class QuadTree(val boundary: Rectangle, val capacity: Int) {

    private val points = arrayListOf<Vertex>()
    private var divided = false
    private var nw: QuadTree? = null
    private var ne: QuadTree? = null
    private var sw: QuadTree? = null
    private var se: QuadTree? = null

    fun insert(p: Vertex): Boolean {
        if(!boundary.contains(p)){
            return false
        }

        return if(points.size < capacity) {
            points.add(p)
            true
        } else {
            if(!divided){
                subdivide()
                divided = true
            }

            nw!!.insert(p) ||
            ne!!.insert(p) ||
            sw!!.insert(p) ||
            se!!.insert(p)
        }
    }

    fun query(range: Rectangle): List<Vertex> {
        if(!boundary.intersects(range)) {
            return listOf()
        }

        var found = points.filter {
            range.contains(it)
        }

        if(divided) {
            found += nw!!.query(range) +
                     ne!!.query(range) +
                     sw!!.query(range) +
                     se!!.query(range)
        }

        return found
    }

    fun query(p: Vec2d, distance: Double): List<Vertex> {
        if(!boundary.contains(p)){
            return listOf()
        }

        var found = points.filter {
            it.dist(p) <= distance
        }

        if(divided) {
            found += nw!!.query(p, distance) +
                    ne!!.query(p, distance) +
                    sw!!.query(p, distance) +
                    se!!.query(p, distance)
        }

        return found
    }

    fun subdivide() {
        with(boundary) {
            nw = QuadTree(Rectangle(x + w / 2, y - h / 2, w / 2, h / 2), capacity)
            ne = QuadTree(Rectangle(x - w / 2, y - h / 2, w / 2, h / 2), capacity)
            sw = QuadTree(Rectangle(x + w / 2, y + h / 2, w / 2, h / 2), capacity)
            se = QuadTree(Rectangle(x - w / 2, y + h / 2, w / 2, h / 2), capacity)
        }
    }

    fun clear() {
        points.clear()
        nw = null
        ne = null
        sw = null
        se = null
    }

    fun render(applet: PApplet) {
        with(applet) {
            push()
                noFill()
                stroke(0f, 255f, 0f)
                rectMode(CENTER)
                rect(boundary.x.toFloat(), boundary.y.toFloat(), boundary.w.toFloat() * 2, boundary.h.toFloat() * 2)
            pop()

            if(divided){
                nw!!.render(this)
                ne!!.render(this)
                sw!!.render(this)
                se!!.render(this)
            }
        }
    }
}