package virusbear.ga2d.problems

import megamu.mesh.Delaunay
import megamu.mesh.Voronoi
import processing.core.PApplet
import virusbear.ga2d.*
import java.util.*
import kotlin.math.sign

fun main(args: Array<String>) {
    PApplet.main(TSP::class.java)
}

class TSP: PApplet() {

    val P = arrayOf(Point(489.81866455078125, 170.24794006347656),
        Point(346.0752868652344, 137.29595947265625),
        Point(481.4361572265625, 154.76522827148438),
        Point(488.8146057128906, 743.2787475585938),
        Point(162.70465087890625, 7.832193374633789),
        Point(658.5956420898438, 308.9139404296875),
        Point(460.6420593261719, 354.75634765625),
        Point(620.7010498046875, 771.1641235351562),
        Point(464.5697937011719, 635.1199340820312),
        Point(124.89967346191406, 530.4005126953125),
        Point(58.57806396484375, 427.3675537109375),
        Point(279.8192443847656, 237.0858154296875),
        Point(566.07861328125, 657.0015258789062),
        Point(416.3958435058594, 568.3204956054688),
        Point(60.34626770019531, 565.3772583007812),
        Point(684.7996826171875, 578.533447265625))

    var tree = arrayOf<Node>()

    override fun settings() {
        size(800, 800, FX2D)
    }

    override fun setup() {
        frameRate(60f)

        tree = P.map { Leaf(it) }.toTypedArray()
    }

    override fun draw() {
        background(0)

        tree.forEach { it.render(this) }

        if(tree.size > 1) {
            tree = buildTree(tree)
        } else {
            buildPath(tree)
            noLoop()
        }

        /*push()
        stroke(255f, 0f, 0f)
        strokeWeight(2f)
        Delaunay(P.map {
            floatArrayOf(it.x.toFloat(), it.y.toFloat())
        }.toTypedArray()).edges.forEach {
            line(it[0], it[1], it[2], it[3])
        }
        pop()*/
    }

    fun buildPath(tree: Array<Node>) {
        val root = tree[0] as Branch

        push()
        val l = divide(root.nodes[0] to root.nodes[1], root)
        pop()


    }

    fun buildTree(tree: Array<Node>): Array<Node> {
        val (_, points) = cPop2D(tree.map { it.p }.toTypedArray())

        return tree.filter {
            it.p != points.first && it.p != points.second
        }.toTypedArray() + Branch((points.first - points.second) / 2 + points.second, tree.filter {
            it.p == points.first || it.p == points.second
        }.toTypedArray())
    }


    fun divide(nodes: Pair<Node, Node>, parent: Branch): List<Leaf> {
        val o = { nodes: Pair<Node, Node>, parent: Branch -> with(nodes) {
            if(first is Leaf && second is Leaf) {
                return@with if(!above(first.p to second.p, parent.p)) { //return
                    listOf(first as Leaf, second as Leaf)
                } else {
                    listOf(second as Leaf, first as Leaf)
                }
            }

            if(first is Branch) {
                val l = divide((first as Branch).nodes[0] to (first as Branch).nodes[1], first as Branch)

                return@with if(second is Leaf) { //return
                    if(above(first.p to second.p, parent.p)){
                        l + listOf(second as Leaf)
                    } else {
                        listOf(second as Leaf) + l
                    }
                } else {
                    val r = divide((second as Branch).nodes[0] to (second as Branch).nodes[1], second as Branch)

                    l + r
                }
            }

            val r = divide((second as Branch).nodes[0] to (second as Branch).nodes[1], second as Branch)

            return@with if(!above(first.p to second.p, parent.p)) { //return
                 r + listOf(first as Leaf)
            } else {
                listOf(first as Leaf) + r
            }
        } }

        val out = o(nodes, parent)

        stroke(0f, 255f, 0f)
        strokeWeight(2f)
        out.forEachIndexed { i, p ->
            val next = out[(i + 1) % out.size]
            line(p.p.x.toFloat(), p.p.y.toFloat(), next.p.x.toFloat(), next.p.y.toFloat())
        }

        saveFrame("D:\\" + System.nanoTime() + ".png")

        return out
    }

    fun rotate(list: List<Leaf>, ref: Point): List<Leaf> {
        Collections.rotate(list, -1 + list.indexOf(list.minBy {
            it.p.dist(ref)
        }))

        if(list[0].p.dist(ref) < list[(list.size - 2) % list.size].p.dist(ref)) {
            Collections.rotate(list, 1)
        }

        return list.map { it }
    }
}

val side = { p: Point, e: Edge -> sign((e.second - e.first).cross(p - e.first)) }

val sameSide = { p1: Point, p2: Point, e: Edge -> side(p1, e) == side(p2, e) }

val above = { p1: Point, e: Edge, p2: Point -> !sameSide(p1, p2, e) }