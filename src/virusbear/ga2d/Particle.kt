package virusbear.ga2d

data class Particle(var pos: Vec2d = Vec2d(0.0, 0.0), var vel: Vec2d = Vec2d(0.0, 0.0), var size: Double = 0.0) {

    fun update() {
        pos += vel
    }
}