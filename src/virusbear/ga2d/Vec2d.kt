package virusbear.ga2d

import virusbear.ga2d.extension.pow
import kotlin.math.*

data class Vec2d(val x: Double, val y: Double) {

    internal constructor(data: DoubleArray): this(data[0], data[1])

    val mag
        get() = sqrt(magSq)

    val magSq
        get() = (x pow 2) + (y pow 2)

    val heading
        get() = atan2(y, x)

    operator fun plus(v: Vec2d) = Vec2d(x + v.x, y + v.y)
    operator fun minus(v: Vec2d) = Vec2d(x - v.x, y - v.y)
    operator fun times(n: Double) = Vec2d(x * n, y * n)
    operator fun times(n: Int) = this * n.toDouble()
    operator fun div(n: Double) = Vec2d(x / n, y / n)
    operator fun div(n: Int) = this / n.toDouble()

    fun dist(v: Vec2d) = (this - v).mag
    fun dot(v: Vec2d) = x * v.x + y * v.y
    fun cross(v: Vec2d) = x * v.y - y * v.x

    fun normalize() = when(mag) {
        0.0, 1.0 -> copy()
        else -> this / mag
    }

    fun limit(max: Double) = when {
        magSq > max pow 2 -> normalize() * max
        else -> copy()
    }

    fun setMag(mag: Double) = normalize() * mag

    fun rotate(theta: Double) = Vec2d(x * cos(theta) - y * sin(theta),
                                      x * sin(theta) + y * cos(theta))

    override fun toString(): String {
        return "[$x, $y]"
    }

    companion object {
        fun fromAngle(theta: Double) = Vec2d(cos(theta), sin(theta))

        fun angleBetween(v1: Vec2d, v2: Vec2d): Double {
            if(v1.magSq == 0.0 || v2.magSq == 0.0) return 0.0

            val amt = v1.dot(v2) / (v1.mag * v2.mag)

            return if(amt >= 1) 0.0 else acos(amt)
        }
    }
}

typealias Vertex = Vec2d
typealias Point = Vec2d

typealias Edge = Pair<Vec2d, Vec2d>