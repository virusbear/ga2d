package virusbear.ga2d

private fun cPop2D(xP: Array<Vec2d>, yP: Array<Vec2d>): Pair<Double, Pair<Vec2d, Vec2d>> {
    val n = xP.size

    if(n <= 3){
        return cPopBruteForce(xP)
    }

    val (dL, pL) = cPop2D(xL(xP, n), yL(yP, xm(xP, n)))
    val (dR, pR) = cPop2D(xR(xP, n), yR(yP, xm(xP, n)))

    val (dmin, pmin) = if(dR < dL) dR to pR else dL to pL
    val yS = yS(yP, xm(xP, n), dmin)
    val nS = yS.size

    var (closest, pclosest) = dmin to pmin

    (0 until (nS-1)).forEach {
        var k = it + 1

        while (k < nS && yS[k].y - yS[it].y < dmin) {
            if(yS[k].dist(yS[it]) < closest) {
                closest = yS[k].dist(yS[it])
                pclosest = yS[k] to yS[it]
            }

            k++
        }
    }

    return closest to pclosest
}

val xL = { xP: Array<Vec2d>, n: Int -> xP.take(n / 2).toTypedArray() }
val xR = { xP: Array<Vec2d>, n: Int -> xP.drop(n / 2).toTypedArray() }
val xm = { xP: Array<Vec2d>, n: Int -> xP[n / 2 - 1].x }
val yL = { yP: Array<Vec2d>, xm: Double -> yP.filter { it.x <= xm }.toTypedArray() }
val yR = { yP: Array<Vec2d>, xm: Double -> yP.filter { it.x > xm }.toTypedArray() }
val yS = { yP: Array<Vec2d>, xm: Double, dmin: Double -> yP.filter { xm - it.x < dmin }.toTypedArray() }

fun cPop2D(cloud: Array<Vec2d>): Pair<Double, Pair<Vec2d, Vec2d>> {
    val xP = cloud.sortedBy { it.x }.toTypedArray()
    val yP = cloud.sortedBy { it.y }.toTypedArray()

    return cPop2D(xP, yP)
}

private fun cPopBruteForce(cloud: Array<Vec2d>): Pair<Double, Pair<Vec2d, Vec2d>> {
    var minDist = (cloud[0] - cloud[1]).mag
    var minPoints = cloud[0] to cloud[1]

    (0..(cloud.size-2)).forEach { i ->
        ((i+1)..(cloud.size-1)).forEach { j ->
            if((cloud[i] - cloud[j]).mag < minDist){
                minDist = (cloud[i] - cloud[j]).mag
                minPoints = cloud[i] to cloud[j]
            }
        }
    }

    return minDist to minPoints
}