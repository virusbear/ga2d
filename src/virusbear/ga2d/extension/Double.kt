package virusbear.ga2d.extension

infix fun Double.pow(exp: Int): Double = (1..(exp-1)).asSequence().fold(this) { v, _ -> v * this }