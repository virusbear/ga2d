package virusbear.ga2d.problems

import processing.core.PApplet
import processing.core.PConstants.HSB
import virusbear.ga2d.Point

abstract class Node(val p: Point) {
    abstract fun render(applet: PApplet)

    abstract fun sort(pos: Point)
}

class Leaf(p: Point): Node(p) {
    override fun render(applet: PApplet) {
        with(applet) {
            push()

            strokeWeight(4f)
            stroke(255f)
            point(p.x.toFloat(), p.y.toFloat())

            pop()
        }
    }

    override fun sort(pos: Point) {
    }
}

class Branch(p: Point, var nodes: Array<Node>): Node(p) {
    override fun render(applet: PApplet) {
        with(applet) {
            push()

            strokeWeight(2f)
            stroke(0f, 0f, 255f)
            line(nodes[0].p.x.toFloat(), nodes[0].p.y.toFloat(), nodes[1].p.x.toFloat(), nodes[1].p.y.toFloat())

            strokeWeight(4f)
            stroke(255f, 0f, 0f)
            point(p.x.toFloat(), p.y.toFloat())

            nodes.forEach {
                it.render(this)
            }

            pop()
        }
    }

    override fun sort(pos: Point) {
        if(nodes[0] is Branch) {
            nodes[0].sort(nodes[1].p)
        }

        if(nodes[1] is Branch) {
            nodes[1].sort(nodes[0].p)
        }

        if((nodes[0].p - pos).magSq < (nodes[1].p - pos).magSq) {
            nodes = arrayOf(nodes[1], nodes[0])
        }
    }

    fun flat(): Array<Point> {
        return (if (nodes[0] is Branch) (nodes[0] as Branch).flat() else arrayOf(nodes[0].p)) + (if (nodes[1] is Branch) (nodes[1] as Branch).flat() else arrayOf(nodes[1].p))
    }
}